package edu.byu.hbll.table;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parses a TSV file according to key columns and value columns and stores that in a map for fast
 * retrieval of values by keys. The keys and the values are be multi-valued and multiple rows can be
 * matched.
 *
 * <p>This class is thread-safe.
 *
 * @author Charles Draper
 */
public class Mapping {

  static final Logger logger = LoggerFactory.getLogger(Mapping.class);

  /** The delimiter used to separate keys in a multivalued key. */
  public static final String KEY_DELIMITER = "\t";

  /** If no key column supplied, the default key column is the first column in the table. */
  public static final int DEFAULT_KEY_COLUMN = 0;

  /** If no value column supplied, the default value column is the second column in the table. */
  public static final int DEFAULT_VALUE_COLUMN = 1;

  /**
   * Concurrent static map to hold all parsed mappings for future retrieval.
   *
   * <p>The key of the map identifies a particular mapping lookup. For example if the tsv file is
   * "table.tsv" and the key column is 0 and you want to pull out the corresponding values in
   * columns 1 and 2, then the mapping key would be "table.tsv\t\t0\t\t1\t2". This uniquely
   * identifies this mapping.
   *
   * <p>The value of the map is all the data for that lookup indexed by the key column(s). If your
   * tsv looked like this
   *
   * <pre>
   * person   color   likes
   * John     red     yes
   * George   blue    yes
   * George   green   no
   * </pre>
   *
   * <p>Then the indexed data would be created like this:
   *
   * <pre>
   * Map&lt;String, String[][]&gt; map = new LinkedHashMap&lt;&gt;();
   * map.put(&quot;person&quot;, new String[][] { { &quot;color&quot;, &quot;likes&quot; } });
   * map.put(&quot;John&quot;, new String[][] { { &quot;red&quot;, &quot;yes&quot; } });
   * map.put(&quot;George&quot;, new String[][] { { &quot;blue&quot;, &quot;yes&quot; }, { &quot;green&quot;, &quot;no&quot; } });
   * </pre>
   *
   * <p>And look like
   *
   * <pre>
   * {person=[[color, likes]], John=[[red, yes]], George=[[blue, yes], [green, no]]}
   * </pre>
   */
  private final Map<String, Map<String, String[][]>> maps = new ConcurrentHashMap<>();

  /** The thread responsible for monitoring directories containing mapping files. */
  private Thread watchThread;

  /** The {@link WatchService} responsible for monitoring directories containing mapping files. */
  private WatchService watchService;

  /** Creates an empty Mapping that does not monitor mapping file changes. */
  public Mapping() {}

  /**
   * Creates an empty Mapping that monitors mapping file changes if the argument is true. If true,
   * will use the default {@link ThreadFactory} to create a new thread for monitoring changes.
   *
   * @param watch whether or not to monitor mapping file changes
   */
  public Mapping(boolean watch) {
    this(Executors.defaultThreadFactory(), watch);
  }

  /**
   * Creates and empty Mapping that monitors mapping file changes. Will use the supplied {@link
   * ThreadFactory} to create a new thread for monitoring changes.
   *
   * @param factory the ThreadFactory to use for creating the monitor thread
   */
  public Mapping(ThreadFactory factory) {
    this(factory, true);
  }

  /**
   * Creates an empty Mapping that monitors mapping file changes if the argument is true. If true,
   * will use the supplied {@link ThreadFactory} to create a new thread for monitoring changes.
   *
   * @param factory the ThreadFactory to use for creating the monitor thread
   * @param watch whether or not to monitor mapping file changes
   */
  protected Mapping(ThreadFactory factory, boolean watch) {
    if (watch) {
      startWatchService(factory);
    }
  }

  /**
   * Gets a value from this mapping.
   *
   * @param filePath the file path of the TSV to parse
   * @param key a value in the first column representing a row in the data
   * @return the value in the second column at that location, or the key if no value is found
   */
  public String get(String filePath, String key) {
    return get(filePath, key, DEFAULT_KEY_COLUMN, DEFAULT_VALUE_COLUMN, key);
  }

  /**
   * Gets a value from this mapping.
   *
   * @param filePath the file path of the TSV to parse
   * @param key a one column key representing a row in the data
   * @param keyColumn which column to lookup on
   * @param valueColumn which column is the value in
   * @param defaultValue a default value if no key is found
   * @return the value or default value found
   */
  public String get(
      String filePath, String key, int keyColumn, int valueColumn, String defaultValue) {
    String[][] values =
        get(
            filePath,
            new String[] {key},
            new int[] {keyColumn},
            new int[] {valueColumn},
            new String[][] {{defaultValue}});

    try {
      return values[0][0];
    } catch (Exception e) {
      return defaultValue;
    }
  }

  /**
   * Gets a value from this mapping.
   *
   * @param filePath the file path of the TSV to parse
   * @param keys a multivalued key representing a row or rows in the data
   * @param keyColumns the columns for the keys
   * @param valueColumns the columns for the values
   * @return a 2d array representing multiple rows that match the key and the values in the
   *     specified columns
   */
  public String[][] get(String filePath, String[] keys, int[] keyColumns, int[] valueColumns) {
    return get(filePath, keys, keyColumns, valueColumns, new String[][] {});
  }

  /**
   * Gets a value from this mapping.
   *
   * @param filePath the file path of the TSV to parse
   * @param keys a multivalued key representing a row or rows in the data
   * @param keyColumns the columns for the keys
   * @param valueColumns the columns for the values
   * @param defaultValues default values when the key is not found
   * @return a 2d array representing multiple rows that match the key and the values in the
   *     specified columns
   */
  public String[][] get(
      String filePath,
      String[] keys,
      int[] keyColumns,
      int[] valueColumns,
      String[][] defaultValues) {

    // generate the key that uniquely identifies this mapping
    String mapKey = getMapKey(filePath, keyColumns, valueColumns);

    // pull the mapping out of the cached maps
    Map<String, String[][]> map = maps.get(mapKey);

    // if the mapping is not there yet, parse the tsv
    if (map == null) {
      try {
        map = loadTsv(new File(filePath), keyColumns, valueColumns);
        maps.put(mapKey, map);
        registerFile(new File(filePath));
      } catch (Exception e) {
        logger.error(
            "Unable to load "
                + Arrays.toString(keyColumns)
                + " => "
                + Arrays.toString(valueColumns)
                + " of mapping file \""
                + filePath
                + "\"",
            e);
      }
    }

    // generate the string representation of a multivalued key
    String key = getKey(keys);

    // get the values for that key
    String[][] values = map.get(key);

    // if the key is not in the map, use the default values
    values = values == null ? defaultValues : values;

    return values;
  }

  /**
   * Generates a key that uniquely identifies a particular mapping.
   *
   * @param filePath the file path of the TSV to parse
   * @param keyColumns the columns for the keys
   * @param valueColumns the columns for the values
   * @return the unique key
   */
  public String getMapKey(String filePath, int[] keyColumns, int[] valueColumns) {
    StringBuilder builder = new StringBuilder(filePath);

    // start the key column section
    builder.append(KEY_DELIMITER);

    for (int keyColumn : keyColumns) {
      builder.append(KEY_DELIMITER);
      builder.append(keyColumn);
    }

    // start the value column section
    builder.append(KEY_DELIMITER);

    for (int valueColumn : valueColumns) {
      builder.append(KEY_DELIMITER);
      builder.append(valueColumn);
    }

    return builder.toString();
  }

  /**
   * Parses a TSV file and puts the contents in the key columns and value columns into a map.
   *
   * @param file the file path of the TSV to parse
   * @param keyColumns the columns for the keys
   * @param valueColumns the columns for the values
   * @return a map representing keys and values
   * @throws IOException if the file cannot be parsed
   */
  public Map<String, String[][]> loadTsv(File file, int[] keyColumns, int[] valueColumns)
      throws IOException {

    logger.info(
        "Loading columns "
            + Arrays.toString(keyColumns)
            + " => "
            + Arrays.toString(valueColumns)
            + " of mapping file \""
            + file
            + "\"");

    // configure the TSV parser
    CSVParser parser = CSVParser.parse(file, StandardCharsets.UTF_8, CSVFormat.TDF);
    Map<String, String[][]> map = new HashMap<>();

    // loop one row at a time
    for (CSVRecord record : parser) {
      // get the values for the keys
      String[] ikeys = getValues(record, keyColumns);

      // get the values for the values
      String[][] ivalues = new String[][] {getValues(record, valueColumns)};

      // generate the unique key for the columns specified
      String ikey = getKey(ikeys);

      // add values to the array if there are multiple values per key
      if (map.containsKey(ikey)) {
        ivalues = ArrayUtils.addAll(map.get(ikey), ivalues);
      }

      map.put(ikey, ivalues);
    }

    return map;
  }

  /**
   * Generates the unique key for a particular row in the data.
   *
   * @param keys the multivalued key
   * @return the concatenated unique key
   */
  public String getKey(String[] keys) {
    StringBuilder builder = new StringBuilder();

    for (String key : keys) {
      builder.append(key);
      builder.append(KEY_DELIMITER);
    }

    return builder.toString();
  }

  /**
   * Pull the values out of a particular row for the given columns. Non-existent cells are treated
   * as empty strings.
   *
   * @param record the row in the data
   * @param columns the columns to extract
   * @return the values at those columns
   */
  public String[] getValues(CSVRecord record, int[] columns) {
    List<String> values = new ArrayList<>();

    for (int column : columns) {
      String value = column < record.size() ? record.get(column) : "";
      values.add(value);
    }

    return values.toArray(new String[values.size()]);
  }

  /**
   * Registers a file's directory to be monitored for changes.
   *
   * @throws IOException if an IO error occurs
   */
  protected void registerFile(File file) throws IOException {
    // assuming register is thread safe and register prevents the same directory
    // from getting registered over and over again
    file.getParentFile()
        .toPath()
        .register(
            watchService,
            StandardWatchEventKinds.ENTRY_CREATE,
            StandardWatchEventKinds.ENTRY_DELETE,
            StandardWatchEventKinds.ENTRY_MODIFY);
  }

  /**
   * Starts up a thread to watch registered directories for any changes. If a change occurs, all
   * mappings are thrown out. They will be reloaded next time they're requested. This thread stays
   * alive until shutdown is called.
   */
  protected void startWatchService(ThreadFactory factory) {
    try {
      watchService = FileSystems.getDefault().newWatchService();

      // create a new thread with the given ThreadFactory
      watchThread =
          factory.newThread(
              // create the runnable that will watch the registered directories for
              // changes
              new Runnable() {
                @Override
                public void run() {
                  // create the new WatchService responsible for watching the
                  // directories and register it to close upon exiting the try block
                  try {
                    // run until shutdown is called
                    while (!Thread.currentThread().isInterrupted()) {
                      // block waiting for the next change
                      final WatchKey key = watchService.take();

                      // remove all events
                      key.pollEvents();

                      // reset the key
                      key.reset();

                      // easiest thing to do is clear out all mappings, they'll get
                      // reloaded next time they're requested
                      maps.clear();
                    }
                  } catch (InterruptedException e) {
                    // do nothing, this is expected
                  } catch (Exception e) {
                    logger.error(e.toString(), e);
                  } finally {
                    try {
                      watchService.close();
                    } catch (IOException e) {
                      logger.warn(e.toString());
                    }
                  }
                }
              });

      // start the watching thread
      watchThread.start();
    } catch (Exception e) {
      logger.error(e.toString(), e);
      shutdown();
    }
  }

  /**
   * Shuts down thread that is watching for changes. Only necessary if {@link Mapping} is set to
   * watch for changes. Does nothing if not watching for changes.
   */
  public void shutdown() {
    if (watchThread != null) {
      watchThread.interrupt();
    }

    if (watchService != null) {
      try {
        watchService.close();
      } catch (IOException e) {
        logger.warn(e.toString());
      }
    }
  }
}
